#!/bin/bash
#set -x

apt update
apt install -y \
           htop linux-headers-`uname -r` \
           build-essential \
           curl \
           apt-transport-https \
           wget \
           lz4

