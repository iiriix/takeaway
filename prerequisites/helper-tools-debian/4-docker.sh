#!/bin/bash

echo "deb [arch=amd64] https://download.docker.com/linux/debian buster stable" > /etc/apt/sources.list.d/docker.list
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
apt update
apt install -y docker-ce docker-ce-cli
docker version

