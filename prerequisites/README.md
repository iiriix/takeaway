# Prerequisites
For the sake of this assignment, I've assumed that the test station would be running a Debian Linux distro. If you're running MacOS, Windows or any other Linux distro, there might be some necessary changes to the instructions below.

---

## Debian
If you are going to install this service on a `Debian Linux 10 (Buster)` machine, you just need to run below scripts with `root` user.



```bash
# Clone the repo and change to this directory
git clone https://gitlab.com/iiriix/takeaway.git
cd prerequisites/helper-tools-debian

# Run below scripts
./1-install_dependencies.sh
./2-kubectl.sh
./3-minikube.sh
./4-docker.sh
./5-create-cluster.sh
```

---

## General Steps For Other Platforms

#### 1. Install Dependencies

Install below packages as well:

    * linux-headers-`uname -r`
    * build-essential
    * curl
    * apt-transport-https # for debian-based distos
    * wget
    * lz4


#### 2. Kubectl
Download and install kubectl:

```bash
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
mv ./kubectl /usr/local/bin/kubectl
kubectl version --client --short
```

#### 3. Minikube
Install `minikube` and run it to have c Kubernetes cluster.

```bash
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
chmod +x ./minikube
mv ./minikube /usr/local/bin/minikube
minkube version
```

#### 4. Docker
Install `docker` on your machine:

```bash
echo "deb [arch=amd64] https://download.docker.com/linux/debian buster stable" > /etc/apt/sources.list.d/docker.list
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
apt update
apt install -y docker-ce docker-ce-cli
docker version
```

#### 5. Create & Start Kubernetes Cluster with Minikube

To create a Kubernetes cluster with `minikube`, please run below command.

```bash
minikube start --vm-driver=none --addons ingress
minikube status
```
