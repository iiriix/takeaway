# BTC Rate Stack
This project is a sample implementation of a service to fetch, store and visualize BTC price. It also provides monitoring and alerting mechanism.

It relies on [Kubernetes](https://kubernetes.io/) ([minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)) in order to create a cluster and deploy the services on it.


## Installation
#### Prerequisites
Make sure all the prerequisites and dependencies are met. Check the [`prerequisites`](prerequisites) directory for more information.

Please also note that you need to run below commands with `root` user, or grant your user the necessary permissions.

#### Build
To build the project run `./ci.sh build`. This will pull several docker images and builds the necessary docker images for the project.

```bash
# Clone the git repository
git clone https://gitlab.com/iiriix/takeaway.git
cd takeaway

# build
./ci.sh build
```

#### Deploy
In order to deploy the services run `./ci.sh deploy`. It might take some time to put all the resources in a running state.

```bash
# deploy
./ci.sh deploy

# To check the status of the deployed resources on kubernetes
kubectl get all
```


## Access To Services

There are two endpoints available after all the resources started successfully. You need to set below domains to resolve to the IP address of the machine that you're running the cluster on that by adding below entries into `/etc/hosts` of your laptop/station.

```bash
# Get minikube address on the cluster
minikube ip

# To resolve domain to minikube IP, add below lines to your local machine
echo "<minikube_ip> airflow.takeaway.iiriix.org" >> /etc/hosts
echo "<minikube_ip> grafana.takeaway.iiriix.org" >> /etc/hosts
```

Now you can access the deployed services using below URLs:

* http://airflow.takeaway.iiriix.org (default user/pass: takeaway/takeaway)
* http://grafana.takeaway.iiriix.org (default user/pass: admin/admin)

## Scale
In order to scale the number of Airflow workers to 2, you need to run:

```bash
kubectl scale deployment airflow-worker --replicas=2
```

## Destroying The Service & Cluster
To remove all the installed resources and the kubernetes cluster:

```bash
./ci.sh destroy
minikube delete
```
