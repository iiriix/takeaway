#!/bin/bash

export AIRFLOW_HOME=/var/lib/airflow
component=$1

case "$component" in
  webserver)
    if [[ ! -f "${AIRFLOW_HOME}/.airflow_db_initialized"  ]]; then
        airflow initdb
        airflow create_user -r $AIRFLOW_ROLE -u $AIRFLOW_USER -e $AIRFLOW_EMAIL -f $AIRFLOW_NAME -l $AIRFLOW_SURNAME -p $AIRFLOW_PASSWORD
        touch "${AIRFLOW_HOME}/.airflow_db_initialized"
    fi
    sleep 2
    exec airflow webserver
    ;;
  scheduler|worker)
    sleep 10
    exec airflow "$@"
    ;;
  *)
    exec "$@"
    ;;
esac
