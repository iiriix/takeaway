#!/usr/bin/env python

import requests
import json
from datetime import timedelta, datetime
from influxdb import InfluxDBClient
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.exceptions import AirflowException

def btcRate(from_currency, to_currency):
    apikey = "8U1MML1FF89EQ4IK"
    url = "https://www.alphavantage.co/query"
    params = {"function": "CURRENCY_EXCHANGE_RATE", \
                  "from_currency": from_currency, \
                  "to_currency": to_currency, \
                  "apikey": apikey}

    try:
        res = requests.get(url, params=params)
        res = json.loads(res.text)['Realtime Currency Exchange Rate']
    except requests.exceptions.RequestException as e:
            print(e)
            sys.exit(1)

    rate = res['5. Exchange Rate']
    time = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
    json_body = [
        {
            "measurement": from_currency.lower() + "_" + to_currency.lower(),
            "tags": {
                "from_currency": from_currency,
                "to_currency": to_currency
            },
            "time": time,
            "fields": {
                "rate": rate
            }
        }
    ]

    try:
        influx_host = "influxdb"
        influx_port = 8086
        influx_user = "admin"
        influx_pass = "admin"
        influx_db = "btc_rate"
        influx_client = InfluxDBClient(influx_host, influx_port, influx_user, influx_pass, influx_db)
        influx_client.create_database(influx_db)
        influx_client.write_points(json_body)
        return "Current rate is: " + rate
    except:
        raise AirflowException("Something went wrong!")

with DAG('btc_rate',
        description='BTC Rate',
        schedule_interval='* * * * *',
        start_date=datetime(2020, 3, 10),
        catchup=False) as dag:
    python_task = PythonOperator(task_id='python_task',
                                python_callable=btcRate,
                                op_args=["BTC", "USD"],
                                email_on_failure=True,
                                email='iiriix@gmail.com')

    python_task
