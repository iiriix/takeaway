#!/bin/bash

influx -username ${INFLUXDB_ADMIN_USER} -password ${INFLUXDB_ADMIN_PASSWORD} -execute " \
    CREATE DATABASE \"telegraf\"; \
    GRANT ALL ON \"telegraf\" TO \"${INFLUXDB_USER}\"; "
