#!/bin/bash

# Exit on any error
set -e

# Apps to be built
buildApps=("grafana" "influxdb" "telegraf" "airflow")
# Apps to be deployed/destroyed
deployApps=("grafana" "influxdb" "telegraf" "redis" "postgres" "airflow")


# Building the images
build() {
  echo "Pulling base images"

  docker pull debian:buster-slim
  docker pull redis:alpine
  docker pull postgres:alpine
  docker pull grafana/grafana:latest
  docker pull influxdb:latest
  docker pull telegraf:latest

  for app in ${buildApps[@]}; do
    echo "Building ${app}..."
    docker build --no-cache -t takeaway/${app}:latest apps/${app}
    echo ""
  done
}

# Test Cases
run-tests() {
  echo "Add tests here"
}

# Deploying to minikube
deploy() {
  echo "Deploying common-tools..."
  kubectl apply -f apps/common-tools/
  echo ""

  for app in ${deployApps[@]}; do
    echo "Deploying ${app}..."
    kubectl apply -f apps/${app}/k8s.yml
    echo ""
  done
}

# Destroying minikube resources
destroy() {
  echo "Destroying common-tools..."
  kubectl delete -f apps/common-tools/
  echo ""

  for app in ${deployApps[@]}; do
    echo "Destroying ${app}..."
    kubectl delete -f apps/${app}/k8s.yml
    echo ""
  done
}

case $1 in
  run-tests)
    run-tests
  ;;
  build)
    build
  ;;
  deploy)
    deploy
  ;;
  destroy)
    destroy
  ;;
  *)
    echo "Usage: $0 {build|run-tests|deploy|destroy}"
    exit 1
  ;;
esac
